export interface IPlayer {
	person: {
		displayName: string;
	}
	unit: string;
	positionAbbr: string;
	position: string;
	depthOrder: number;
}
