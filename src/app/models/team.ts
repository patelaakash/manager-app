import { IPlayer } from './roster';

export interface ITeam {
	id: string;
	name: string;
	fullName: string;
	nickName: string;
	abbr: string;
	roster: IPlayer[];
}
