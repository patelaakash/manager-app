import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"; import { TeamsComponent } from "./components/teams/teams.component";
import { RosterComponent } from "./components/roster/roster.component";
import { PlayerComponent } from './components/player/player.component';
import { HttpClientModule } from "@angular/common/http";
import { MaterialModule } from "./modules/material.module";
import { MiddlewareService } from './services/middleware.service';

@NgModule({
  declarations: [
    AppComponent,
    TeamsComponent,
    RosterComponent,
    PlayerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [MiddlewareService],
  bootstrap: [AppComponent]
})
export class AppModule { }
