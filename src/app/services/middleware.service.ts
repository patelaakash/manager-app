import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ITeam } from '../models/team';
import { IPlayer } from '../models/roster';

@Injectable({
  providedIn: 'root'
})
export class MiddlewareService {
  apiURL = './assets/data.json';
  teams: ITeam[];
  selectedPlayer: IPlayer;

  constructor(private httpClient: HttpClient) { }

  getTeams(): Observable<any> {
    return this.httpClient.get<any>(this.apiURL);
  }

  setTeamData(teamData: ITeam[]) {
    this.teams = teamData;
  }

  getIndividualTeam(id: string) {
    return this.teams.find(team => {
      return team.id === id;
    });
  }
}
