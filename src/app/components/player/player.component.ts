import { MiddlewareService } from './../../services/middleware.service';
import { Component, OnInit } from '@angular/core';
import { IPlayer } from 'src/app/models/roster';
import { Location } from '@angular/common';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  selectedPlayer: IPlayer;

  constructor(private middlewareService: MiddlewareService, private location: Location) { }

  onCancel() {
    this.location.back();
  }

  onSave() {

  }

  ngOnInit() {
    this.selectedPlayer = this.middlewareService.selectedPlayer;
  }

}
