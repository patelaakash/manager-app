import { MiddlewareService } from './../../services/middleware.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ITeam } from 'src/app/models/team';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
  teams: any;
  selectedTeam: ITeam;
  message: string;

  goToRoster(team: ITeam) {
    this.selectedTeam = team;
    this.router.navigate(['team', this.selectedTeam.id]);
  }

  constructor(
    private middlewareService: MiddlewareService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.middlewareService.getTeams().subscribe((data) => {
      this.teams = data.teams;
      this.middlewareService.setTeamData(this.teams);
    });
  }
}
