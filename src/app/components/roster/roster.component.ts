import { MiddlewareService } from './../../services/middleware.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from '@angular/router';
import { IPlayer } from 'src/app/models/roster';
import { ITeam } from 'src/app/models/team';
@Component({
  selector: 'app-roster',
  templateUrl: './roster.component.html',
  styleUrls: ['./roster.component.scss']
})

export class RosterComponent implements OnInit {

  team: ITeam;
  selectedPlayer: IPlayer;
  message: string;
  teamId: string;

  editPlayer(player) {
    this.selectedPlayer = player;
    this.middlewareService.selectedPlayer = this.selectedPlayer;
    this.router.navigate(['/player']);
  }

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private middlewareService: MiddlewareService) {
  }

  ngOnInit() {
    this.teamId = this.activatedRoute.snapshot.params.id;
    this.team = this.middlewareService.getIndividualTeam(this.teamId);
    if (this.team) {
      this.team.roster.sort((a, b) => {
        return a.depthOrder > b.depthOrder ? 1 : -1;
      });
    }
  }
}
